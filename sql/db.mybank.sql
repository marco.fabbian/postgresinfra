

-- Database: Bank

-- DROP DATABASE IF EXISTS "my-bank";

CREATE DATABASE "my-bank"
    WITH
    OWNER = postgres
    ENCODING = 'UTF8'
    LC_COLLATE = 'en_US.utf8'
    LC_CTYPE = 'en_US.utf8'
    TABLESPACE = pg_default
    CONNECTION LIMIT = -1
    IS_TEMPLATE = False;

GRANT TEMPORARY, CONNECT ON DATABASE "my-bank" TO PUBLIC;

GRANT ALL ON DATABASE "my-bank" TO postgres;

\c my-bank

-- SCHEMA: customeraccount

-- DROP SCHEMA IF EXISTS customeraccount ;

CREATE SCHEMA IF NOT EXISTS customeraccount
    AUTHORIZATION postgres;


-- Table: customeraccount.account

-- DROP TABLE IF EXISTS customeraccount.account;

CREATE TABLE IF NOT EXISTS customeraccount.account
(
    id uuid NOT NULL,
    brid uuid,
    type character varying COLLATE pg_catalog."default",
    currency character varying COLLATE pg_catalog."default",
    CONSTRAINT account_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS customeraccount.account
    OWNER to postgres;


-- SCHEMA: customerinformation

-- DROP SCHEMA IF EXISTS customerinformation ;

CREATE SCHEMA IF NOT EXISTS customerinformation
    AUTHORIZATION postgres;


-- Table: customerinformation.address

-- DROP TABLE IF EXISTS customerinformation.address;

CREATE TABLE IF NOT EXISTS customerinformation.address
(
    id uuid NOT NULL,
    street character varying COLLATE pg_catalog."default",
    city character varying COLLATE pg_catalog."default",
    province character varying COLLATE pg_catalog."default",
    country character varying COLLATE pg_catalog."default",
    CONSTRAINT address_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS customerinformation.address
    OWNER to postgres;


-- Table: customerinformation.customer

-- DROP TABLE IF EXISTS customerinformation.customer;

CREATE TABLE IF NOT EXISTS customerinformation.customer
(
    id uuid NOT NULL,
    brid uuid,
    name character varying COLLATE pg_catalog."default",
    firstname character varying COLLATE pg_catalog."default",
    secondname character varying COLLATE pg_catalog."default",
    addressid bigint,
    CONSTRAINT customer_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS customerinformation.customer
    OWNER to postgres;


-- SCHEMA: financialinstrument

-- DROP SCHEMA IF EXISTS financialinstrument ;

CREATE SCHEMA IF NOT EXISTS financialinstrument
    AUTHORIZATION postgres;


-- Table: financialinstrument.isin

-- DROP TABLE IF EXISTS financialinstrument.isin;

CREATE TABLE IF NOT EXISTS financialinstrument.isin
(
    id uuid NOT NULL,
    code character varying COLLATE pg_catalog."default",
    description character varying COLLATE pg_catalog."default",
    CONSTRAINT "Isin_pkey" PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS financialinstrument.isin
    OWNER to postgres;


-- SCHEMA: taxationservice

-- DROP SCHEMA IF EXISTS taxationservice ;

CREATE SCHEMA IF NOT EXISTS taxationservice
    AUTHORIZATION postgres;


-- Table: taxationservice.taxation

-- DROP TABLE IF EXISTS taxationservice.taxation;

CREATE TABLE IF NOT EXISTS taxationservice.taxation
(
    id uuid NOT NULL,
    countrycode character varying COLLATE pg_catalog."default",
    taxprofile numeric,
    taxcode character varying COLLATE pg_catalog."default",
    CONSTRAINT tax_pkey PRIMARY KEY (id)
)

TABLESPACE pg_default;

ALTER TABLE IF EXISTS taxationservice.taxation
    OWNER to postgres;